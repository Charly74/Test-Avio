interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: '9SfgVscJUzKPTWF8DzlDVpgzaKaOfBgF',
  domain: 'charlysosa.auth0.com',
  callbackURL: 'http://localhost:4200/callback'
};
