import { NgModule } from '@angular/core';
import { UserIdFilterPipe } from './user-id-filter.pipe';

@NgModule({
  imports: [],
  declarations: [UserIdFilterPipe],
  exports: [UserIdFilterPipe],
})

export class UserIdFilterModule {

  static forRoot() {
    return {
      ngModule: UserIdFilterModule,
      providers: [],
    };
  }
}
