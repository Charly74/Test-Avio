import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';

import { TranslateService } from 'ng2-translate/ng2-translate';
import { ToasterModule, ToasterService, ToasterConfig } from 'angular2-toaster';

import { Profile, ProfileService, Toaster, CallToasterService, ToasterTypes } from './shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private toasterService: ToasterService;
  profile: Profile;
  profile_picture = '../assets/loading.svg';
  selected = 'es';

  public config: ToasterConfig =
    new ToasterConfig({
      showCloseButton: false,
      tapToDismiss: true,
      timeout: 3000
    });

  constructor(public auth: AuthService,
    public profileService: ProfileService,
    private translate: TranslateService,
    toasterService: ToasterService,
    private callToaster: CallToasterService) {
    auth.handleAuthentication();
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');
    this.toasterService = toasterService;
  }



  ngOnInit() {
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    if (localStorage.getItem('idioma') != null) {
      this.translate.use(localStorage.getItem('idioma'));
    } else {
      this.translate.use('es');
      localStorage.setItem('idioma', 'es');
    }

    this.profile = {
      family_name: '',
      gender: '',
      given_name: '',
      locale: '',
      name: '',
      nickname: '',
      picture: '',
      sub: '',
      updated_at: ''
    };

    if (this.auth.isAuthenticated()) {
      this.getProfile();
    } else {
      this.logoutProfile();
    }

    this.profileService.getProfile$.subscribe(
      () => {
        this.getProfile();
      }
    );

    this.profileService.getProfile$.subscribe(
      () => {
        this.logoutProfile();
      }
    );

    this.callToaster.toaster$.subscribe(
      (toaster: Toaster) => {
        this.toasterService.pop(ToasterTypes[toaster.type], toaster.title, toaster.message);
      }
    );
  }

  getProfile() {
    this.profile_picture = '../assets/loading.svg';
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
      this.profile_picture = this.profile.picture;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
        this.profile_picture = this.profile.picture;
      });
    }
  }

  logoutProfile() {
    this.profile_picture = null;
    this.profile = {
      family_name: '',
      gender: '',
      given_name: '',
      locale: '',
      name: '',
      nickname: '',
      picture: '',
      sub: '',
      updated_at: ''
    };
  }

  setLanguage(i) {
    this.translate.use(i);
    localStorage.setItem('idioma', i);
  }
}
