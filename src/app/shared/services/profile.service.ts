import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ProfileService {
  private getProfile = new Subject();
  private logoutProfile = new Subject();
  getProfile$ = this.getProfile.asObservable();
  logoutProfile$ = this.logoutProfile.asObservable();

  constructor() { }

  sendGetProfile() {
    this.getProfile.next();
  }

  sendLogoutProfile() {
    this.logoutProfile.next();
  }
}
