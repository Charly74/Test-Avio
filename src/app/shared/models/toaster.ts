export class Toaster {
  type: number;
  title: string;
  message: string;
}
