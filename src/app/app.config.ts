export const config = {
    'ServerApis': {
      'UrlSite': 'https://jsonplaceholder.typicode.com/',
      'Urls': {
        'getItems': 'posts'
      }
    },
    'MaxSizeImgInsumos': 102400
  };
