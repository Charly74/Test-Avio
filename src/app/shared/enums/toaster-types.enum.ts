export enum ToasterTypes {
  success = 1,
  info = 2,
  warning = 3,
  error = 4
}
