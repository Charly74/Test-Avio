import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { config } from '../app.config';
import { CallToasterService, Item, ItemsService, ToasterTypes } from '../shared';
import { TranslateService } from 'ng2-translate/ng2-translate';

import * as _ from 'underscore';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-modal-item',
  templateUrl: './modal-item.component.html',
  styleUrls: ['./modal-item.component.css']
})
export class ModalItemComponent implements OnInit {
  item: Item = new Item();
  modalTitle: string;
  itemForm: FormGroup;
  toastSuccess: string;
  toastError: string;
  toastAddSuccessMsg: string;
  toastUpdateSuccessMsg: string;
  toastAddErrorMsg: string;
  toastUpdateErrorMsg: string;

  constructor(private itemsService: ItemsService,
    fb: FormBuilder,
    private translate: TranslateService,
    private callToasterService: CallToasterService) {
    this.itemForm = fb.group({
      'userId': [null],
      'id': new FormControl({ value: null, disabled: true }),
      'title': [null],
      'body': [null]
    });
  }

  ngOnInit() {
    this.itemsService.editItem$.subscribe(
      (item: Item) => {
        this.item = _.clone(item);
        this.translate.get('ITEMS', { value: 'EDIT_ITEM' }).subscribe((res) => {
          this.modalTitle = res.EDIT_ITEM;
        });
        this.itemForm.setValue({
          userId: this.item.userId,
          id: this.item.id,
          title: this.item.title,
          body: this.item.body
        });
      }
    );

    this.itemsService.newItem$.subscribe(
      () => {
        this.translate.get('ITEMS', { value: 'NEW_ITEM' }).subscribe((res) => {
          this.modalTitle = res.NEW_ITEM;
        });
        this.item = {
          userId: null,
          id: null,
          title: null,
          body: null
        };
        this.itemForm.setValue({
          userId: null,
          id: null,
          title: null,
          body: null
        });
      }
    );
  }

  cancelItem() {
    $('#modal_item').modal('hide');
  }

  saveItem() {
    const saveItem: Item = {
      userId: this.itemForm.controls['userId'].value,
      id: this.item.id,
      title: this.itemForm.controls['title'].value,
      body: this.itemForm.controls['body'].value
    };

    this.translate.get('ITEMS', { value: 'ITEM_ADDED' }).subscribe((res) => {
      this.toastAddSuccessMsg = res.ITEM_ADDED;
    });
    this.translate.get('ITEMS', { value: 'ITEM_UPDATED' }).subscribe((res) => {
      this.toastUpdateSuccessMsg = res.ITEM_UPDATED;
    });
    this.translate.get('ITEMS', { value: 'ITEM_ADDED_ERROR' }).subscribe((res) => {
      this.toastAddErrorMsg = res.ITEM_ADDED_ERROR;
    });
    this.translate.get('ITEMS', { value: 'ITEM_UPDATED_ERROR' }).subscribe((res) => {
      this.toastUpdateErrorMsg = res.ITEM_UPDATED_ERROR;
    });
    this.translate.get('GENERAL', { value: 'SUCCESS' }).subscribe((res) => {
      this.toastSuccess = res.SUCCESS;
    });
    this.translate.get('GENERAL', { value: 'ERROR' }).subscribe((res) => {
      this.toastError = res.ERROR;
    });

    if (!saveItem.id) {
      this.itemsService.sendAddItem(saveItem);
      this.callToasterService.showToaster({ type: ToasterTypes.success, title: this.toastSuccess, message: this.toastAddSuccessMsg });
    } else {
      this.itemsService.sendUpdateItem(saveItem);
      this.callToasterService.showToaster({ type: ToasterTypes.success, title: this.toastSuccess, message: this.toastUpdateSuccessMsg });
    }

    $('#modal_item').modal('hide');
  }
}
