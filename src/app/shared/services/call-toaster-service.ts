import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { Toaster } from '../models/toaster';

@Injectable()
export class CallToasterService {
  // Observable Toaster source
  private toaster = new Subject<Toaster>();

  // Observable Alert stream
  toaster$ = this.toaster.asObservable();
  constructor() { }

  showToaster(toaster: Toaster) {
    this.toaster.next(toaster);
  }
}
