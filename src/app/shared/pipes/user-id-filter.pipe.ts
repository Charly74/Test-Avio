import { Pipe, PipeTransform } from '@angular/core';

import { Item } from '../models/item';

@Pipe({
  name: 'userIdFilter',
  pure: false
})
export class UserIdFilterPipe implements PipeTransform {
  transform(items: Item[], filter: Item): Item[] {
    if (!items || !filter) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((item: Item) => this.applyFilter(item, filter));
  }

  /**
   * Perform the filtering.
   *
   * @param {Item} item The api to compare to the filter.
   * @param {Item} filter The filter to apply.
   * @return {boolean} True if api satisfies filters, false if not.
   */
  applyFilter(item: Item, filter: Item): boolean {
    // tslint:disable-next-line:forin
    for (const field in filter) {
      if (filter[field]) {
        if (filter[field] === 'null') {
          return true;
        }
        if (typeof filter[field] === 'string') {
          if (item[field].toString().toLowerCase().indexOf(filter[field].toLowerCase()) === -1) {
            return false;
          }
        } else if (typeof filter[field] === 'number') {
          if (item[field] !== filter[field]) {
            return false;
          }
        }
      }
    }
    return true;
  }
}
