export enum ToasterPositions {
  'toast-top-right' = 1,
  'toast-bottom-right' = 2,
  'toast-bottom-left' = 3,
  'toast-top-left' = 4,
  'toast-top-full-width' = 5,
  'toast-bottom-full-width' = 6,
  'toast-top-center' = 7,
  'toast-bottom-center' = 8
}
