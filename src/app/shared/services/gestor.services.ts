import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { config } from '../../app.config';

@Injectable()
export class GestorService {
  constructor(
    private http: HttpClient
  ) { }

  HttpGet(url: string, parameters: Object) {
    return new Promise<any>((resolve, reject) => {
      this.http.get(config.ServerApis.UrlSite + url)
        .toPromise()
        .then(res => resolve(res))
        .catch(this.handleErrorPromise);
    });
  }

  HttpPost(url: string, parameters: Object) {
    return new Promise<any>((resolve, reject) => {
      this.http.post(config.ServerApis.UrlSite + url,
        parameters, { responseType: 'text' }
      )
        .toPromise()
        .then((res) => resolve(res))
        .catch(this.handleErrorPromise);
    });
  }

  HttpPut(url: string, parameters: Object, id: number) {
    return new Promise<any>((resolve, reject) => {
      this.http.put(config.ServerApis.UrlSite + url + '/' + id.toString(),
        parameters, { responseType: 'text' }
      )
        .toPromise()
        .then((res) => resolve(res))
        .catch(this.handleErrorPromise);
    });
  }

  private handleErrorPromise(error: Response | any) {
    console.log(error.message || error);
    if (error.status === 0) {
      // window.location.href="/login";
    } else {
      return Promise.reject(error.message || error);
    }
  }
}
