import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth/auth.service';

import { Item, ItemsService } from '../shared';
import { TranslateService } from 'ng2-translate/ng2-translate';

import * as _ from 'underscore';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  items: Array<Item>;
  search: Item = new Item();

  constructor(public auth: AuthService,
    public itemService: ItemsService,
    private translate: TranslateService) { }

  ngOnInit() {
    if (this.auth.isAuthenticated()) {
      this.getItems();
    }

    this.itemService.addItem$.subscribe(
      (item: Item) => {
        const aux = _.max(this.items, function (i) {
          return i.id;
        });
        item.id = aux.id + 1;
        this.items.push(item);
      }
    );

    this.itemService.updateItem$.subscribe(
      (item: Item) => {
        this.items = _.without(this.items, _.findWhere(this.items, {
          id: item.id
        }));
        this.items.push(item);
        this.items = _.sortBy(this.items, 'id');
      }
    );

    this.itemService.deleteItemArray$.subscribe(
      (item: Item) => {
        this.items = _.without(this.items, _.findWhere(this.items, {
          id: item.id
        }));
      }
    )
  }

  getItems() {
    this.itemService.getItems()
      .then((res: Array<Item>) => {
        this.items = res;
      })
  }

  editItem(i) {
    this.itemService.sendEditItem(i);
    $('#modal_item').modal('show');
  }

  newItem() {
    this.itemService.sendNewItem();
    $('#modal_item').modal('show');
  }

  deleteItem(i) {
    this.itemService.sendDeleteItem(i);
    $('#modal_delete_item').modal('show');
  }
}
