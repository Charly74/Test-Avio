import { Component, OnInit } from '@angular/core';

import { Item, ItemsService } from '../shared';
import { TranslateService } from 'ng2-translate/ng2-translate';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-modal-delete-item',
  templateUrl: './modal-delete-item.component.html',
  styleUrls: ['./modal-delete-item.component.css']
})
export class ModalDeleteItemComponent implements OnInit {

  item: Item;
  constructor(public itemService: ItemsService,
    private translate: TranslateService) { }

  ngOnInit() {
    this.itemService.deleteItem$.subscribe(
      (item: Item) => {
        this.item = item;
      }
    );
  }

  cancel() {
    $('#modal_delete_item').modal('hide');
  }

  save() {
    this.itemService.sendDeleteItemArray(this.item);
    $('#modal_delete_item').modal('hide');
  }
}
