import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { config } from '../../app.config';
import { GestorService } from './gestor.services';
import { Item } from '../models';

@Injectable()
export class ItemsService {

  private newItem = new Subject();
  private updateItem = new Subject<Item>();
  private editItem = new Subject<Item>();
  private addItem = new Subject<Item>();
  private deleteItem = new Subject<Item>();
  private deleteItemArray = new Subject<Item>();

  newItem$ = this.newItem.asObservable();
  updateItem$ = this.updateItem.asObservable();
  editItem$ = this.editItem.asObservable();
  addItem$ = this.addItem.asObservable();
  deleteItem$ = this.deleteItem.asObservable();
  deleteItemArray$ = this.deleteItemArray.asObservable();

  item: Item = new Item();
  items: Array<Item>;

  constructor(public gestor: GestorService) { }

  sendNewItem() {
    this.newItem.next(this.item);
  }

  sendUpdateItem(item: Item) {
    this.updateItem.next(item);
  }

  sendEditItem(item: Item) {
    this.editItem.next(item);
  }

  sendAddItem(item: Item) {
    this.addItem.next(item);
  }

  sendDeleteItem(item: Item) {
    this.deleteItem.next(item);
  }

  sendDeleteItemArray(item: Item) {
    this.deleteItemArray.next(item);
  }

  getItems() {
    return new Promise((resolve, reject) => {
      this.gestor.HttpGet(config.ServerApis.Urls.getItems, {})
        .then((res: Array<Item>) => {
          if (res) {
            this.items = res;
            resolve(this.items);
          } else {
            console.log('Se produjo un error');
            reject('error');
          }
        })
        .catch(error => {
          console.log(error);
          reject(error);
        });
    });
  }
}
